import java.util.Scanner;

/**
 * Created by Jakub on 2014-12-08.
 */
public class NewtonAlghoritmAPI {

    public static void loadData() {

        System.out.println("Podaj stopien wielomianu");
        Scanner in = new Scanner(System.in);
        int polynomialDegree = in.nextInt() + 1;

        Polynomial pol = null;

        for(int i = 0; i < polynomialDegree; i++) {
            System.out.println("podaj wspolczynniki i potęge " + (i+1) + " jednomianu składowego");
            int wspolczynnik = in.nextInt();
            int potega = in.nextInt();
            if(pol == null) {
                pol = new Polynomial(wspolczynnik, potega);
            } else {
               pol = pol.plus(new Polynomial(wspolczynnik, potega));
            }
        }
        System.out.println("Wprowadzony wielomian" + pol);

        System.out.println("Podaj x1");
        int x1 = in.nextInt();

        System.out.println("Podaj x2");
        int x2 = in.nextInt();

        System.out.println("Podaj epsilon");
        int epsilon = in.nextInt();
        try {
            NewtonAlghoritm alghoritm = new NewtonAlghoritm(pol, x1, x2, epsilon);
            System.out.println("Wynik to: " +  alghoritm.getResult() + " osiagniety podczas " + alghoritm.getIterationsNumber() + " iteracji");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {

        loadData();
    }
}
